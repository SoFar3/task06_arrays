package com.epam.game;

import com.epam.game.controller.GameController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        GameController gameController = new GameController();
        gameController.start();
    }

}
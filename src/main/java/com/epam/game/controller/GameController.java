package com.epam.game.controller;

import com.epam.game.model.Door;
import com.epam.game.model.MagicArtifact;
import com.epam.game.model.Monster;
import com.epam.game.model.Room;
import com.epam.game.view.Menu;
import com.epam.game.view.Option;
import com.epam.game.view.View;

import java.util.List;

public class GameController {

    private Room room;
    private View view;

    public GameController() {
        view = new View();
        room = new Room();

        Menu doorMenu = new Menu(1);

        doorMenu.addMenuOption("1", new Option("Random doors", () -> {
            room.generate();
        }));

        doorMenu.addMenuOption("2", new Option("Manual doors", () -> {
            Menu subMenu = new Menu(doorMenu, 3);

            subMenu.addMenuOption("1", new Option("Monster", () -> {
                room.addDoor(new Door(new Monster(Integer.parseInt(view.userInput("Enter monster power[5 - 100]: ")))));
            }));

            subMenu.addMenuOption("2", new Option("Magic artifact", () -> {
                room.addDoor(new Door(new MagicArtifact(Integer.parseInt(view.userInput("Enter artifact power[10 - 80]: ")))));
            }));

            subMenu.addMenuOption("q", new Option("Back", () -> {
                view.setMenu(subMenu.getParent());
            }));

            view.setMenu(subMenu);
        }));

        Menu menu = new Menu(20);

        menu.addMenuOption("1",
                new Option("Show all doors",
                        () -> room.getDoors().forEach(System.out::println)));
        menu.addMenuOption("2",
                new Option("Win doors order",
                        () -> {
                            List<Integer> integers = room.winDoorOrder();
                            if (integers.size() < 10) {
                                System.out.println("There is no way that you can win");
                            } else {
                                System.out.println(integers);
                            }
                        }));
        menu.addMenuOption("3",
                new Option("Amount of death door",
                        () -> System.out.println(room.countDeathDoor())));
        menu.addMenuOption("q",
                new Option("Quit",
                        () -> System.exit(0)));

        doorMenu.addMenuOption("n", new Option("Next menu", () -> {
            view.setMenu(menu);
        }));

        view.setMenu(doorMenu);
    }

    public void start() {
        view.showMenu();
    }

}

package com.epam.game.model;

import com.epam.game.App;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class GameTree {

    private static final Logger logger = LogManager.getLogger(App.class);

    private Node root;

    private Room room;

    public GameTree(Room room) {
        this.room = room;
    }

    public void buildGameTree() {
        root = new Node();
        ArrayList<Door> closedDoors = new ArrayList<>(room.getDoors());
        buildGameTree0(root, closedDoors);
    }

    private void buildGameTree0(Node node, ArrayList<Door> closedDoors) {
        for (int i = 0; i < closedDoors.size(); i++) {
            if ( ! room.getHero().isAlive()) {
                return;
            }
            Door door = closedDoors.get(i);

            Hero hero = new Hero(room.getHero().getPower());

            door.getGameObject().interact(room.getHero());

            node.nodes.add(new Node());
            node.door = door;

            closedDoors.remove(i);

            buildGameTree0(node.nodes.get(node.nodes.size() - 1), closedDoors);
            closedDoors.add(door);

            room.setHero(hero);
        }
    }

    public void printTree() {
        printTree0(root);
    }

    private void printTree0(Node root) {
        /*if (nodes.size() == 9) {
            System.out.println(nodes);
        }
        for (int i = 0; i < node.nodes.size(); i++) {
            nodes.add(node.nodes.get(i));
            logger.warn(node.nodes.get(i));
            printTree0(node.nodes.get(i), nodes);
            nodes.remove(node.nodes.get(i));
        }*/
        if (root == null)
            return;

        Queue<Node > q = new LinkedList<>(); // Create a queue
        q.add(root); // Enqueue root
        while (!q.isEmpty())
        {
            int n = q.size();

            // If this node has children
            while (n > 0)
            {
                // Dequeue an item from queue
                // and print it
                Node p = q.peek();
                q.remove();
                System.out.print(p.door.getNum() + " ");

                // Enqueue all children of
                // the dequeued item
                for (int i = 0; i < p.nodes.size(); i++)
                    q.add(p.nodes.get(i));
                n--;
            }

            // Print new line between two levels
            System.out.println();
        }
    }

    private static class Node {

        Door door;
        List<Node> nodes;

        public Node() {
            this.door = new Door();
            this.nodes = new ArrayList<>();
        }

        @Override
        public String toString() {
            return door.toString();
        }

    }

}
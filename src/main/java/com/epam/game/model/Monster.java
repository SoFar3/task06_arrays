package com.epam.game.model;

import java.util.Random;

public class Monster extends GameObject {

    public Monster() {
        this.power = new Random().nextInt(95) + 5;
    }

    public Monster(int power) {
        super(power);
    }

    public void interact(Hero hero) {
        if (hero.getPower() < this.power) {
            hero.die();
        }
    }

    @Override
    public String toString() {
        return "Monster{" +
                "power=" + power +
                "}";
    }

}

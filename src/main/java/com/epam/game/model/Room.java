package com.epam.game.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Room {

    private List<Door> doors;
    private Hero hero;

    public Room() {
        doors = new ArrayList<>();
        hero = new Hero();
    }

    public void generate() {
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int choice = random.nextInt(2);
            doors.add(
                    new Door(choice == 1 ? new Monster() : new MagicArtifact()
                            , i)
            );
        }
    }

    public void addDoor(Door door) {
        doors.add(door);
    }

    public List<Integer> winDoorOrder() {
        List<Integer> doorNum = new ArrayList<>();
        int maxPower = 25;
        for (Door door : doors) {
            if (door.getGameObject() instanceof MagicArtifact) {
                maxPower += door.getGameObject().power;
                doorNum.add(door.getNum());
            }
        }
        for (Door door : doors) {
            if (door.getGameObject() instanceof Monster) {
                if (door.getGameObject().power > maxPower) {
                    break;
                }
                doorNum.add(door.getNum());
            }
        }
        return doorNum;
    }

    public int countDeathDoor() {
        int counter = 0;
        for (Door door : doors) {
            door.getGameObject().interact(hero);
            if (! hero.isAlive()) {
                counter++;
            }
            hero.reset();
        }
        return counter;
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public List<Door> getDoors() {
        return doors;
    }

    @Override
    public String toString() {
        return "Room{\n" +
                doors + "\n" +
                '}';
    }

}

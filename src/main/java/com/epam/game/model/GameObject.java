package com.epam.game.model;

public abstract class GameObject {

    protected int power;

    public GameObject() {
    }

    public GameObject(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }

    public abstract void interact(Hero hero);

    @Override
    public String toString() {
        return "GameObject{" +
                "power=" + power +
                '}';
    }

}

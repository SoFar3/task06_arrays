package com.epam.game.model;

public class Door {

    private int num;
    private GameObject gameObject;

    public Door() {
    }

    public Door(GameObject gameObject) {
        this.gameObject = gameObject;
    }

    public Door(GameObject gameObject, int num) {
        this(gameObject);
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public GameObject getGameObject() {
        return gameObject;
    }

    public void setGameObject(GameObject gameObject) {
        this.gameObject = gameObject;
    }

    @Override
    public String toString() {
        return "Door{" +
                "num=" + num + " " +
                gameObject +
                "}";
    }

}

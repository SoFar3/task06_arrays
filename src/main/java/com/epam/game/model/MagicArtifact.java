package com.epam.game.model;

import java.util.Random;

public class MagicArtifact extends GameObject {

    public MagicArtifact() {
        this.power = new Random().nextInt(70) + 10;
    }

    public MagicArtifact(int power) {
        super(power);
    }

    public void interact(Hero hero) {
        hero.powerUp(this.power);
    }

    @Override
    public String toString() {
        return "MagicArtifact{" +
                "power=" + power +
                "}";
    }

}

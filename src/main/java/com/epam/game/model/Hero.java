package com.epam.game.model;

public class Hero {

    private static final int HERO_DEFAULT_POWER = 25;

    private int power;

    private boolean alive = true;

    public Hero() {
        this.power = HERO_DEFAULT_POWER;
    }

    public Hero(int power) {
        this.power = power;
    }

    public void reset() {
        alive = true;
        power = HERO_DEFAULT_POWER;
    }

    public void die() {
        alive = false;
    }

    public boolean isAlive() {
        return alive;
    }

    public int getPower() {
        return power;
    }

    public void powerUp(int buff) {
        this.power += buff;
    }

}

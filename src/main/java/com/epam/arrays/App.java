package com.epam.arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class App {

    private static Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 3, 4, 5, 7, 9));
        ArrayList<Integer> b = new ArrayList<>(Arrays.asList(1, 2, 4, 6, 7, 8, 9));
        logger.debug(intersection(a, b));
        logger.debug(subtraction(a, b));

        ArrayList<Integer> c = new ArrayList<>(Arrays.asList(1, 1, 4, 1, 2, 2, 4, 8, 8, 8, 9, 8, 7, 7));
        logger.debug(deleteDuplicates(c));
        logger.debug(deleteAllDuplicates(c));
    }

    public static ArrayList<Integer> intersection(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> inter = new ArrayList<>();
        for (int i = 0; i < a.size(); i++) {
            for (int j = 0; j < b.size(); j++) {
                if (a.get(i).equals(b.get(j))) {
                    inter.add(a.get(i));
                    break;
                }
            }
        }
        return inter;
    }

    public static ArrayList<Integer> subtraction(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> sub = new ArrayList<>();
        for (int i = 0; i < a.size(); i++) {
            boolean same = false;
            for (int j = 0; j < b.size(); j++) {
                if (a.get(i).equals(b.get(i))) {
                    same = true;
                    break;
                }
            }
            if ( ! same) {
                sub.add(a.get(i));
            }
        }
        return sub;
    }

    public static ArrayList<Integer> deleteAllDuplicates(ArrayList<Integer> a) {
        ArrayList<Integer> org = new ArrayList<>(a);
        Map<Integer, Integer> numFrequence = new HashMap<>();
        for (int i = org.size() - 1; i >= 0; i--) {
            numFrequence.put(org.get(i), numFrequence.get(org.get(i)) == null ? 1 : numFrequence.get(org.get(i)) + 1);
        }
        for (int i = org.size() - 1; i >= 0; i--) {
            if (numFrequence.get(org.get(i)) > 2) {
                org.remove(i);
            }
        }
        return org;
    }

    public static ArrayList<Integer> deleteDuplicates(ArrayList<Integer> a) {
        ArrayList<Integer> org = new ArrayList<>(a);
        for (int i = org.size() - 1; i >= 0; i--) {
            if ((i - 1 >= 0) && org.get(i).equals(org.get(i - 1))) {
                org.remove(i);
            }
        }
        return org;
    }

}

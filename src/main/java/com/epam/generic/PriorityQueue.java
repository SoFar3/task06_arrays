package com.epam.generic;

public class PriorityQueue <T extends Comparable<T>> {

    private Node<T> head;

    private int size = 0;

    public void push(T value) {
        Node<T> tempHead = head;
        Node<T> temp = new Node<>(value, null);
        if (head == null) {
            head = temp;
        } else {
            if (value.compareTo(head.value) <= 0) {
                temp.next = head;
                head = temp;
                size++;
                return;
            }
            while (tempHead.next != null && tempHead.next.value.compareTo(value) <= 0) {
                tempHead = tempHead.next;
            }
            Node<T> tempRef = tempHead.next;
            tempHead.next = temp;
            temp.next = tempRef;
        }
        size++;
    }

    public T pop() {
        Node<T> temp = head;
        head = head.next;
        size--;
        return temp.value;
    }

    public int size() {
        return size;
    }

    private static class Node<T> {

        T value;
        Node<T> next;

        public Node(T value, Node<T> next) {
            this.value = value;
            this.next = next;
        }

    }

}

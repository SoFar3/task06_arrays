package com.epam.generic;

import java.util.ArrayList;

public class UnitsContainer<T extends Number> {

    private ArrayList<T> units;

    public UnitsContainer() {
        this.units = new ArrayList<>();
    }

    public void add(T unit) {
        units.add(unit);
    }

    public T get(int index) {
        return units.get(index);
    }

    public int size() {
        return units.size();
    }

}

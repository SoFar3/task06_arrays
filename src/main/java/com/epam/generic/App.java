package com.epam.generic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class App {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        unitsContainerSample();
        priorityQueueSample();
        stringToIntegerSample();
    }

    public static void priorityQueueSample() {
        logger.debug("Priority queue sample");

        PriorityQueue<Integer> priorityIntegers = new PriorityQueue<>();
        priorityIntegers.push(5);
        priorityIntegers.push(1);
        priorityIntegers.push(8);
        priorityIntegers.push(4);
        priorityIntegers.push(3);

        for (int i = priorityIntegers.size() - 1; i >= 0; i--) {
            logger.debug("Pop element from queue: " + priorityIntegers.pop());
        }
    }

    public static void unitsContainerSample() {
        logger.debug("Units container sample");

        UnitsContainer<Integer> integerUnitsContainer = new UnitsContainer<>();
        integerUnitsContainer.add(5);
        integerUnitsContainer.add(6);

        for (int i = 0; i < integerUnitsContainer.size(); i++) {
            logger.debug(integerUnitsContainer.get(i));
        }

    }

    public static void stringToIntegerSample() {
        logger.debug("String to integers sample");

        String str = "Hello";
        logger.debug(stringToIntegers(str));
    }

    public static List<Integer> stringToIntegers(String str) {
        List<Integer> intString = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            intString.add((int)str.charAt(i));
        }
        return intString;
    }

}

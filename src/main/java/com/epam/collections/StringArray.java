package com.epam.collections;

import com.epam.arrays.App;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StringArray {

    private static Logger logger = LogManager.getLogger(App.class);

    private static final int DEFAULT_ARRAY_SIZE = 10;
    private String[] stringArray;
    private int counter = 0;

    public StringArray() {
        stringArray = new String[DEFAULT_ARRAY_SIZE];
    }

    public void add(String str) {
        if (counter == stringArray.length) {
            resize();
        }
        stringArray[counter++] = str;
        logger.debug("Element added. Element: " + str);
    }

    public String get(int index) {
        return stringArray[index];
    }

    private void resize() {
        String[] newArr = new String[stringArray.length * 2];
        System.arraycopy(this.stringArray, 0, newArr, 0, this.stringArray.length);
        this.stringArray = newArr;
        logger.debug("Array has been resized. Size: " + stringArray.length);
    }

}

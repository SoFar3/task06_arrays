package com.epam.collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

public class App {

    private static Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        dequeSample();
        countryCapitalSample();
        stringArraySample();
    }

    public static void dequeSample() {
        logger.debug("Deque sample");

        EpamDeque<Integer> deque = new EpamDeque<>();
        deque.addFirst(5);
        deque.addFirst(4);
        deque.addFirst(3);
        deque.addLast(4);
        deque.addLast(3);

        for (int i = deque.size() - 1; i >= 0; i--) {
            logger.debug("Element poll: " + deque.pollFirst());
        }
    }

    public static void stringArraySample() {
        logger.debug("String array sample");
        StringArray stringArray = new StringArray();
        for (int i = 0; i < 15; i++) {
            stringArray.add(i + "");
        }
    }

    public static void countryCapitalSample() {
        logger.debug("Country capital sample");
        ArrayList<CountryCapital> countryCapitals = Generator.generate(5);
        Collections.sort(countryCapitals);
        logger.debug("Sorted by Country");
        printList(countryCapitals);

        countryCapitals.sort(new CountryCapital.CapitalComparator());
        logger.debug("Sorted by Capital");
        printList(countryCapitals);
    }

    private static void printList(List<CountryCapital> list) {
        for (CountryCapital countryCapital : list) {
            logger.debug(String.format("%-16s - %-10s", countryCapital.getCountry(), countryCapital.getCapital()));
        }
    }

}

package com.epam.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class EpamDeque<T> {

    private Node<T> head;
    private Node<T> tail;

    private boolean isEmpty = true;
    private int size = 0;

    public void addFirst(T t) {
        Node<T> temp = new Node<>(t, null, null);
        if (head == null) {
            head = temp;
            tail = temp;
        } else {
            head.prev = temp;
            temp.next = head;
            head = temp;
        }
        size++;
        isEmpty = false;
    }

    public void addLast(T t) {
        Node<T> temp = new Node<>(t, null, null);
        if (head.next == null) {
            head.next = tail;
        }
        if (tail == null) {
            tail = temp;
            head = temp;
        } else {
            tail.next = temp;
            temp.prev = tail;
            tail = temp;
        }
        size++;
        isEmpty = false;
    }

    public boolean offerFirst(T t) {
        //stub
        return false;
    }

    public boolean offerLast(T t) {
        //stub
        return false;
    }

    public T removeFirst() {
        if ( ! isEmpty) {
            Node<T> temp = head;
            head = head.next;
            size--;
            checkSize();
            return temp.value;
        } else {
            throw new NoSuchElementException();
        }
    }

    public T removeLast() {
        if ( ! isEmpty) {
            Node<T> temp = tail;
            tail = tail.prev;
            size--;
            checkSize();
            return temp.value;
        } else {
            throw new NoSuchElementException();
        }
    }

    public T pollFirst() {
        if (head != null) {
            Node<T> temp = head;
            head = head.next;
            if (head != null) {
                head.prev = null;
            }
            size--;
            checkSize();
            return temp.value;
        } else {
            return null;
        }
    }

    public T pollLast() {
        if (tail != null) {
            Node<T> temp = tail;
            tail = tail.prev;
            if (tail != null) {
                tail.next = null;
            }
            size--;
            checkSize();
            return temp.value;
        } else {
            return null;
        }
    }

    public T getFirst() {
        if ( ! isEmpty) {
            return head.value;
        } else {
            throw new NoSuchElementException();
        }
    }

    public T getLast() {
        if ( ! isEmpty) {
            return tail.value;
        } else {
            throw new NoSuchElementException();
        }
    }

    public T peekFirst() {
        return head.value;
    }

    public T peekLast() {
        return tail.value;
    }

    public boolean removeFirstOccurrence(Object o) {
        //stub
        return false;
    }

    public boolean removeLastOccurrence(Object o) {
        //stub
        return false;
    }

    public boolean add(T t) {
        addLast(t);
        return true;
    }

    public boolean offer(T t) {
        //stub
        return false;
    }

    public T remove() {
        return removeFirst();
    }

    public T poll() {
        return pollFirst();
    }

    public T element() {
        return getFirst();
    }

    public T peek() {
        return getFirst();
    }

    public void push(T t) {
        addFirst(t);
    }

    public T pop() {
        return pollFirst();
    }

    public boolean remove(Object o) {
        //stub
        return false;
    }

    public boolean containsAll(Collection<?> c) {
        //stub
        return false;
    }

    public boolean addAll(Collection<? extends T> c) {
        //stub
        return false;
    }

    public boolean removeAll(Collection<?> c) {
        //stub
        return false;
    }

    public boolean retainAll(Collection<?> c) {
        //stub
        return false;
    }

    public void clear() {

    }

    public boolean contains(Object o) {
        return false;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public Iterator<T> iterator() {
        //stub
        return null;
    }

    public Object[] toArray() {
        //stub
        return new Object[0];
    }

    public <T1> T1[] toArray(T1[] a) {
        //stub
        return null;
    }

    public Iterator<T> descendingIterator() {
        //stub
        return null;
    }

    private void checkSize() {
        if (size == 0) {
            isEmpty = true;
        }
    }

    private static class Node<T> {

        private T value;
        private Node<T> next;
        private Node<T> prev;

        public Node(T value, Node<T> next, Node<T> prev) {
            this.value = value;
            this.next = next;
            this.prev = prev;
        }

    }

}

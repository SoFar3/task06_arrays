package com.epam.collections;

import java.util.Comparator;

public class CountryCapital implements Comparable<CountryCapital> {

    private String country;
    private String capital;

    public CountryCapital(String country, String capital) {
        this.country = country;
        this.capital = capital;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    @Override
    public int compareTo(CountryCapital o) {
        return this.country.compareTo(o.country);
    }

    public static class CapitalComparator implements Comparator<CountryCapital> {

        @Override
        public int compare(CountryCapital o1, CountryCapital o2) {
            return o1.capital.compareTo(o2.capital);
        }

    }

    @Override
    public String toString() {
        return "{" +
                "country='" + country +
                ", capital='" + capital +
                "}\n";
    }

}

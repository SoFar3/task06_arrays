package com.epam.collections;

import java.util.ArrayList;
import java.util.Random;

public class Generator {

    public static ArrayList<CountryCapital> countryCapital = new ArrayList<>();

    static {
        countryCapital.add(new CountryCapital("Canada", "Ottawa"));
        countryCapital.add(new CountryCapital("Colombia", "Bogota"));
        countryCapital.add(new CountryCapital("Egypt", "Cairo"));
        countryCapital.add(new CountryCapital("Ghana", "Accra"));
        countryCapital.add(new CountryCapital("Georgia", "Tbilisi"));
        countryCapital.add(new CountryCapital("India", "New Delhi"));
        countryCapital.add(new CountryCapital("Italy", "Rome"));
        countryCapital.add(new CountryCapital("Japan", "Tokyo"));
        countryCapital.add(new CountryCapital("Kenya", "Nairobi"));
        countryCapital.add(new CountryCapital("Liberia", "Monrovia"));
        countryCapital.add(new CountryCapital("Mali", "Bamako"));
        countryCapital.add(new CountryCapital("New Zealand", "Wellington"));
        countryCapital.add(new CountryCapital("Russia", "Moscow"));
        countryCapital.add(new CountryCapital("Ukraine", "Kiev"));
        countryCapital.add(new CountryCapital("Venezuela", "Caracas"));
        countryCapital.add(new CountryCapital("United States", "Washington D.C."));
    }

    public static ArrayList<CountryCapital> generate(int size) {
        if (size > countryCapital.size()) {
            throw new IllegalArgumentException();
        }
        ArrayList<CountryCapital> gen = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            gen.add(countryCapital.get(random.nextInt(countryCapital.size())));
        }
        return gen;
    }

}
